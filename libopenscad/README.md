# libopenscad


## home, sweet home

    git@gitlab.com:m5658/libopenscad.git

## adopt me

    git subtree add  --message 'mtools git subtree add locator - libopenscad\nrepo=git@gitlab.com:m5658/libopenscad.git\nprefix=libopenscad\nbranch=main' --prefix libopenscad  git@gitlab.com:m5658/libopenscad.git main --squash

## inherit updates from the mother ship

    git subtree pull --prefix libopenscad git@gitlab.com:m5658/libopenscad.git main --squash

## push our updates to the mother ship

    git subtree push --prefix libopenscad git@gitlab.com:m5658/libopenscad.git main --squash


